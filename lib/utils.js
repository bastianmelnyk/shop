export const formatPrice = (language, price) => {
  switch (language) {
    case 'de':
      return price.toFixed(2).replace('.', ',') + ' €'
    default:
      return '€ ' + price.toFixed(2)
  }
}

export const loadCategories = language => {
  return import('~/products/categories.json').then(
    ({ default: categoryIds }) => {
      return Promise.all(
        categoryIds.map(id =>
          import(`~/products/${id}/category.yml.json`).then(
            ({ default: categoryData }) => {
              const title = categoryData.title[language]
              return {
                id,
                title,
                path: `/${language}/${id}`
              }
            }
          )
        )
      )
    }
  )
}

export const loadProduct = (language, category, id) =>
  Promise.all([
    import(`~/products/${category}/${id}/product.yml.json`).then(
      ({ default: productData }) => productData
    ),
    import(`~/products/${category}/${id}/images.json`).then(
      ({ default: images }) => images
    )
  ]).then(([productData, images]) => {
    const { available } = productData
    const title = productData.title[language]

    const description = productData.description[language].split(/\n/)
    description.pop()

    const price = formatPrice(language, productData.price)

    return {
      id,
      title,
      description,
      price,
      available,
      path: `/${language}/${category}/${id}`,
      images: (images || []).map(name =>
        require(`~/static/products/${category}/${id}/${name}`)
      )
    }
  })

export const loadCategoryProducts = (language, category) => {
  return import(`~/products/${category}/products.json`).then(
    ({ default: productIds }) => {
      return Promise.all(
        (productIds || []).map(id => loadProduct(language, category, id))
      )
    }
  )
}
