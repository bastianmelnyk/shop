#!/usr/bin/env bash

set -o errexit
set -o nounset

cd $(dirname $0)/..

for f in $(find products -name product.yml -or -name category.yml)
do
  echo "Converting $f ..."
  yarn --silent js-yaml $f > $f.json
done

for f in $(find products -name product.yml)
do
  product_directory=$(dirname $f)
  echo "Writing $product_directory/images.json ..."
  (cd $product_directory && find *.png -exec echo "- {}" \;) | yarn --silent js-yaml > $product_directory/images.json
done

for f in $(find products -name category.yml)
do
  category_directory=$(dirname $f)
  echo "Writing $category_directory/products.json ..."

  (
    cd $category_directory;
    for p in $(find * -name product.yml)
    do
      echo "- $(dirname $p)"
    done
  ) | yarn --silent js-yaml > $category_directory/products.json
done

echo "Writing categories.json ..."
(
  cd products;
  for f in $(find * -name category.yml)
  do
    echo "- $(dirname $f)"
  done
) | yarn --silent js-yaml > products/categories.json

for f in $(find products -name '*.png')
do
  echo "Copying $f ..."
  mkdir -p $(dirname static/$f)
  cp $f static/$f
done
